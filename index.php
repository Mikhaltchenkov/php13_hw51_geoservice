<?php

error_reporting(E_ERROR);
$loader = require_once __DIR__.'/vendor/autoload.php';

if (!empty($_GET['address'])) {
    $address = $_GET['address'];
} else {
    $address = 'г. Донецк, ул. Взлётная, 1';
}
?>
<html>
<head>
    <title>My GEO service</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="//api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
<!--    <script src="icon_customImage.js" type="text/javascript"></script>-->
</head>
<body>
<div class="container">
    <h2>Поиск координат по адресу:</h2>
    <form action="index.php" method="get">
        <div class="form-group">
            <div class="row">
                <div class="col-md-10"><input type="text" class="form-control" name="address" value="<?= $address ?>"
                                              placeholder="Введите искомый адрес"></div>
                <div class="col-md-2"><input type="submit" class="btn btn-primary" name="submit" value="Найти"></div>
            </div>
        </div>
    </form>
    <?php
    $api = new \Yandex\Geo\Api();
    $api->setQuery($address);

    // Настройка фильтров
    $api
        ->setLimit(5) // кол-во результатов
        ->setLang(\Yandex\Geo\Api::LANG_RU) // локаль ответа
        ->load();

    $response = $api->getResponse();
    // Список найденных точек
    $collection = $response->getList();
    $jsarray = '<script type="text/javascript">var coords = [';
?>
    <p>Результатов: <?= count($collection) ?></p>
    <table class="table table-hover">
        <thead>
        <tr>
            <th>Координаты</th>
            <th>Адрес</th>
            <th>Операции</th>
        </tr>
        </thead>
        <tbody>

    <?php
    $index = 0;
    foreach ($collection as $item) {
        $lat =  $item->getLatitude(); // широта
        $lon = $item->getLongitude(); // долгота
        echo "<tr><td nowrap>$lat, $lon</td><td>" . $item->getAddress();
        echo "</td><td><button class='btn btn-info' onclick='myMap.setCenter([$lat, $lon])'>Показать</button></td></tr>";
        if ($index++!=0) {
            $jsarray .=', ';
        }
        $jsarray .= "[$lat, $lon]";
    }
    $jsarray .= '];</script>';
    ?>
        </tbody>
    </table>
    <?= $jsarray?>
    <div id="map" style="width: 600px; height: 400px"></div>
    <script type="text/javascript">
        ymaps.ready(init);
        var myMap;

        function init(){
            myMap = new ymaps.Map("map", {
                center: coords[0],
                zoom: 10
            });

            for (var i = 0; i < coords.length; i++) {
                 myMap.geoObjects.add(new ymaps.Placemark(coords[i], {
                     hintContent: '<?= $address ?>'}));
            }
        }

    </script>
</div>
</body>
</html>
